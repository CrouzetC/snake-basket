Snake Basket
============

A sample project to experiment Python packaging.

Usage :

	(Windows)  .\\activate

	(Linux)  .venv/Scripts/activate

	python main.py

Then you can leave the environment with :

	deactivate
